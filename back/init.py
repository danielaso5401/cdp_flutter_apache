from flask import Flask, render_template
from flask import request
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root@127.0.0.1/flaskmysql'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Candidato(db.Model):
    idCandidato = db.Column(db.Integer, primary_key=True)
    candidato_name = db.Column(db.String(100))
    candidato_partpol = db.Column(db.String(100))

    def __init__(self, candidato_name, candidato_partpol):
        self.candidato_name = candidato_name
        self.candidato_partpol = candidato_partpol

class Elector(db.Model):
    id_elector = db.Column(db.Integer, primary_key=True)
    elector_name = db.Column(db.String(100))
    elector_dni = db.Column(db.String(8))

    def __init__(self,elector_name,elector_dni):
        self.elector_name=elector_name
        self.elector_dni=elector_dni

db.create_all()

class CandidatoSchema(ma.Schema):
    class Meta:
        fields = ("idCandidato","candidato_name", "candidato_partpol")

candidato_schema = CandidatoSchema()
candidato_schemas = CandidatoSchema(many=True)

class ElectorSchema(ma.Schema):
    class Meta:
        fields = ('id_elector',"elector_name","elector_dni")

elector_schema = ElectorSchema()
elector_schemas = ElectorSchema(many=True)

@app.route('/create_candidato', methods=['POST'])
def create_candidato():
    print(request.json)
    candidato_name=request.json["candidato_name"]
    candidato_partpol=request.json["candidato_partpol"]

    new_candidato = Candidato(candidato_name, candidato_partpol)
    db.session.add(new_candidato)
    db.session.commit()

    return candidato_schema.jsonify(new_candidato)

@app.route('/get_candidato', methods=['GET'])
def candidatos():
    all_candidatos = Candidato.query.all()
    result = candidato_schemas.dump(all_candidatos)
    return jsonify(result)

@app.route('/create_elector', methods=['POST'])
def create_elector():
    print(request.json)
    elector_name=request.json["elector_name"]
    elector_dni=request.json["elector_dni"]


    new_elector = Elector(elector_name,elector_dni)

    db.session.add(new_elector)
    db.session.commit()

    return elector_schema.jsonify(new_elector)

@app.route('/get_elector', methods=['GET'])
def electores():
    all_electores = Elector.query.all()
    result = elector_schemas.dump(all_electores)
    return jsonify(result)

@app.route('/delete_elector/<ide>', methods=['DELETE'])
def eliminar_elector(ide):
	deleteelector=Elector.query.filter_by(id_elector=ide).one()
	db.session.delete(deleteelector)
	db.session.commit()
	return "eliminado correctamente"

@app.route('/update_elector/<ide>', methods=["PUT"])
def actualizar_elector(ide):
	elector_name=request.json["elector_name"]
	elector_dni=request.json["elector_dni"]
	elector_id=Elector.query.filter_by(id_elector=ide).one()
	elector_id.elector_name=elector_name
	elector_id.elector_dni=elector_dni
	db.session.commit()
	return "acutalizacion correcta"


@app.route('/')
def home():
    return render_template("index.html")

#if __name__ == "__main__":
#    app.debug = True
