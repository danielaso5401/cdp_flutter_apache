import 'package:cdp/paginas/act_elec.dart';
import 'package:cdp/paginas/lis_elect.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Inicio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  Widget centrar(a) {
    return Container(
      child: Center(
        child: Column(
          children: [a],
        ),
      ),
    );
  }

  Widget logo(url) {
    return Image.network(
      url,
      width: 120,
      height: 120,
    );
  }

  Widget titulo() {
    return Text("VOTO AQP",
        style: TextStyle(
            color: Colors.white, fontSize: 35.0, fontWeight: FontWeight.bold));
  }

  Widget boton(String text, BuildContext context, pagina) {
    return RaisedButton(
        shape: StadiumBorder(),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 13),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => pagina));
        },
        child: Text(
          text,
          style: TextStyle(fontSize: 20, color: Colors.black),
        ));
  }

  Widget enviar(
      String text, BuildContext context, pagina, campo1, campo2, opcion) {
    return RaisedButton(
        shape: StadiumBorder(),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 13),
        onPressed: () {
          String envio1 = campo1.text;
          String envio2 = campo2.text;
          print(envio2 + "   " + envio1);
          post_app(envio1, envio2, opcion);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => pagina));
        },
        child: Text(
          text,
          style: TextStyle(fontSize: 20, color: Colors.black),
        ));
  }

  Widget campo(entrada, valor) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
      child: TextField(
        controller: valor,
        decoration: InputDecoration(
          hintText: entrada,
          fillColor: Colors.black26,
          filled: true,
          hintStyle: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget eliminar(ide, context) {
    return RaisedButton(
        shape: StadiumBorder(),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 9, vertical: 9),
        onPressed: () {
          String valor = ide.toString();
          delete_app(valor);
          Navigator.pop(context);
        },
        child: Text(
          "Eliminar",
          style: TextStyle(fontSize: 15, color: Colors.black),
        ));
  }

  Widget actualizar(ide, name, dni, context) {
    return RaisedButton(
        shape: StadiumBorder(),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 9, vertical: 9),
        onPressed: () {
          String valor = ide.toString();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Actualizar_elect(valor, name, dni)));
        },
        child: Text(
          "Actualizar",
          style: TextStyle(fontSize: 15, color: Colors.black),
        ));
  }

  Widget confirmar(ide, name, dni, context) {
    return RaisedButton(
        shape: StadiumBorder(),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 9, vertical: 9),
        onPressed: () {
          update_app(ide, name, dni);
          Navigator.pop(context);
        },
        child: Text(
          "Confirmar",
          style: TextStyle(fontSize: 15, color: Colors.black),
        ));
  }

  void update_app(valor, name, dni) async {
    var url = Uri.parse("http://192.168.0.26:5000/update_elector/$valor");
    final http.Response response = await http.put(url,
        headers: {
          "content-type": "application/json",
        },
        body: jsonEncode({
          "elector_name": name,
          "elector_dni": dni,
        }));
  }

  void delete_app(String ide) async {
    var url = Uri.parse("http://192.168.0.26:5000/delete_elector/$ide");
    final http.Response response = await http.delete(url);
  }

  void post_app(campo1, campo2, opcion) async {
    if (opcion == 1) {
      var url = Uri.parse("http://192.168.0.26:5000/create_candidato");
      var response = await http.post(url,
          headers: {
            "content-type": "application/json",
          },
          body: jsonEncode({
            "candidato_name": campo1,
            "candidato_partpol": campo2,
          }));
    } else {
      var url = Uri.parse("http://192.168.0.26:5000/create_elector");
      var response = await http.post(url,
          headers: {
            "content-type": "application/json",
          },
          body: jsonEncode({
            "elector_name": campo1,
            "elector_dni": campo2,
          }));
    }
  }
}
