import 'dart:convert';

import 'package:cdp/inicio.dart';
import 'package:cdp/modelos/Candidato.dart';
import 'package:cdp/paginas/lis_cand.dart';
import 'package:cdp/paginas/lis_elect.dart';
import 'package:cdp/paginas/reg_cand.dart';
import 'package:cdp/paginas/reg_elector.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],
        accentColor: Colors.cyan[600],
        fontFamily: 'Montserrat',
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: MyHomePage(title: 'VOTOS'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: 100),
            Inicio().centrar(Inicio().logo(
                'https://upload.wikimedia.org/wikipedia/commons/1/1e/RPC-JP_Logo.png')),
            Inicio().centrar(Inicio().titulo()),
            SizedBox(height: 40),
            Inicio().centrar(
                Inicio().boton("Registro candidato", context, Candidato())),
            SizedBox(height: 20),
            Inicio().centrar(
                Inicio().boton("Registro elector", context, Elector())),
            SizedBox(height: 20),
            Inicio().centrar(Inicio()
                .boton("Lista de Candidatos", context, ListaCandidatos())),
            SizedBox(height: 20),
            Inicio().centrar(
                Inicio().boton("Lista de Electores", context, ListaElector())),
          ],
        ),
      ),
    );
  }
}
