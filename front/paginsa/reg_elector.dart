import 'package:flutter/material.dart';

import '../inicio.dart';
import '../main.dart';

class Elector extends StatefulWidget {
  @override
  _Elector createState() => _Elector();
}

class _Elector extends State<Elector> {
  final f_name = TextEditingController();
  final f_dni = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: const Text('Registro de Elector',
            style: TextStyle(color: Colors.white70, fontSize: 24)),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(height: 30),
                Inicio().logo(
                    "https://image.flaticon.com/icons/png/512/3135/3135715.png"),
                SizedBox(height: 40),
                Inicio().campo("Nombre del elector", f_name),
                Inicio().campo("DNI", f_dni),
                SizedBox(height: 30),
                Inicio()
                    .enviar("enviar", context, MyHomePage(), f_name, f_dni, 2)
              ],
            ),
          )
        ],
      ),
    );
  }
}
