import 'package:cdp/inicio.dart';
import 'package:cdp/main.dart';
import 'package:flutter/material.dart';

class Candidato extends StatefulWidget {
  @override
  _Candidato createState() => _Candidato();
}

class _Candidato extends State<Candidato> {
  final f_name = TextEditingController();
  final f_part = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: const Text('Registro de Candidatos',
            style: TextStyle(color: Colors.white70, fontSize: 24)),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(height: 30),
                Inicio().logo(
                    "https://image.flaticon.com/icons/png/512/1283/1283231.png"),
                SizedBox(height: 40),
                Inicio().campo("Nombre del candidato", f_name),
                Inicio().campo("Nombre del Partido", f_part),
                SizedBox(height: 30),
                Inicio()
                    .enviar("enviar", context, MyHomePage(), f_name, f_part, 1)
              ],
            ),
          )
        ],
      ),
    );
  }
}
