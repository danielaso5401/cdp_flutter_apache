import 'package:cdp/inicio.dart';
import 'package:cdp/modelos/Candidato.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ListaCandidatos extends StatefulWidget {
  @override
  _ListaCandidatos createState() => _ListaCandidatos();
}

class _ListaCandidatos extends State<ListaCandidatos> {
  Future<List<Cand>> _listadoCandidato;
  Future<List<Cand>> _getCandidatos() async {
    var url = Uri.parse("http://192.168.0.26:5000/get_candidato");
    final respuesta = await http.get(url);
    List<Cand> cand = [];
    if (respuesta.statusCode == 200) {
      String cuerpo = utf8.decode(respuesta.bodyBytes);
      final jasodata = jsonDecode(cuerpo);
      for (var i in jasodata) {
        cand.add(Cand(i["candidato_name"], i["candidato_partpol"]));
      }
      return cand;
    } else {
      throw Exception("Fallo la conexion");
    }
  }

  @override
  void initState() {
    super.initState();
    _listadoCandidato = _getCandidatos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de candidatos"),
      ),
      body: FutureBuilder(
        future: _listadoCandidato,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView(
              children: lista(snapshot.data),
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return Text("error");
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  List<Widget> lista(List<Cand> data) {
    List<Widget> candi = [];
    for (var i in data) {
      candi.add(Card(
          child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
              "Candidato:  " + i.name + "\n Partido Politico: " + i.partido,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold)),
        ),
      ])));
    }
    return candi;
  }
}
