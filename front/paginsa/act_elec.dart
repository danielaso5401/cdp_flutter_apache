import 'package:cdp/inicio.dart';
import 'package:cdp/modelos/Candidato.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

final f_name = TextEditingController();
final f_dni = TextEditingController();
String ide2;
String name2;
String dni2;

class Actualizar_elect extends StatefulWidget {
  Actualizar_elect(ide, name, dni) {
    ide2 = ide;
    name2 = name;
    dni2 = dni;
  }
  @override
  _Actualizar_elect createState() => _Actualizar_elect();
}

class _Actualizar_elect extends State<Actualizar_elect> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Actualizar Elector"),
        ),
        body: Column(
          children: [
            SizedBox(height: 20),
            Inicio().campo(name2, f_name),
            Inicio().campo(dni2, f_dni),
            SizedBox(height: 40),
            Inicio().confirmar(ide2, name2, dni2, context)
          ],
        ));
  }
}
