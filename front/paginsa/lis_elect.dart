import 'package:cdp/inicio.dart';
import 'package:cdp/modelos/Elector.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ListaElector extends StatefulWidget {
  @override
  _ListaElector createState() => _ListaElector();
}

class _ListaElector extends State<ListaElector> {
  Future<List<Elect>> _listadoCandidato;
  Future<List<Elect>> _getCandidatos() async {
    var url = Uri.parse("http://192.168.0.26:5000/get_elector");
    final respuesta = await http.get(url);
    List<Elect> cand = [];
    if (respuesta.statusCode == 200) {
      String cuerpo = utf8.decode(respuesta.bodyBytes);
      final jasodata = jsonDecode(cuerpo);
      for (var i in jasodata) {
        cand.add(Elect(i["elector_name"], i["elector_dni"], i["id_elector"]));
      }
      return cand;
    } else {
      throw Exception("Fallo la conexion");
    }
  }

  @override
  void initState() {
    super.initState();
    _listadoCandidato = _getCandidatos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de Electores"),
      ),
      body: FutureBuilder(
        future: _listadoCandidato,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView(
              children: lista(snapshot.data),
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return Text("error");
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  List<Widget> lista(List<Elect> data) {
    List<Widget> candi = [];
    for (var i in data) {
      candi.add(Card(
          child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Elector:  " + i.name + "\n DNI: " + i.dni,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold)),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Inicio().eliminar(i.id, context),
              SizedBox(width: 20),
              Inicio().actualizar(i.id, i.name, i.dni, context)
            ],
          ),
        ),
      ])));
    }
    return candi;
  }
}
